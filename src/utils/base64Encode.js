const convertBase64String = (file) => {
  const encodedString = btoa(JSON.stringify(file));
  return encodedString;
};

export default convertBase64String;
