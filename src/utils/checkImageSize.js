const checkImageFileSize = (file) => {
  return new Promise((resolve, reject) => {
    const isLt5M = file.size / 1024 / 1024 <= 5;
    if (!isLt5M) {
      messageApi.open({
        type: "error",
        content: `Image must smaller than 5MB!`,
      });
      setFileList([]);
      reject(false);
    } else {
      resolve(true);
    }
  });
};

export default checkImageFileSize;
