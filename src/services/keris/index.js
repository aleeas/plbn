import axios from 'axios'
import { fetcher, API } from '../index'

export const matchData = async (token, payload)=> {
  try {
    const { data: response } = await fetcher.post(API.UPLOAD.POST.KERIS(token), payload);
    return response;
  } catch (error) {
    if (axios.isAxiosError(error)) {
      throw error.response?.data;
    }
    throw error;
  }
};

