import axios from "axios";
import { getToken, removeToken } from "../lib/cookies";

const baseURL = process.env.NEXT_PUBLIC_BASE_URL;

const Axios = axios.create({
  baseURL,
  timeout: 60000,
});

Axios.interceptors.request.use(
  (request) => {
    const token = getToken();

    if (request.headers === undefined) {
      request.headers;
    }

    if (token) {
      request.headers.Authorization = `Bearer ${token}`;
    } else {
      request.headers.Authorization = "";
    }

    return request;
  },
  (error) => {
    Promise.reject(error);
  }
);

Axios.interceptors.response.use(
  (response) => response,
  (error) => {
    const errorResponse = {
      ...error,
      response: {
        ...error.response,
        error: true,
      },
    };

    if (
      (error?.response?.status === 401 || error?.response?.status === 403) &&
      !error?.response?.data?.message_client.includes("Invalid Password")
    ) {
      removeToken();
      localStorage.clear();

      if (typeof window !== "undefined") {
        window.location.reload();
      }

      setTimeout(() => {
        window.location.href = "/auth/login";
      }, 2000);
    }

    if (error.response === undefined || error.response === "undefined") {
      return Promise.reject(errorResponse);
    }
    if (error.response.status) {
      return Promise.reject(errorResponse);
    }
  }
);

export default Axios;
