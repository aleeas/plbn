import {
  FileOutlined,
  UserOutlined,
} from "@ant-design/icons";

const sidebarMenu = [
  {
    label: "Pakuan",
    key: "pakuan",
    path: "/pakuan",
    icon: <UserOutlined />,
    children: null,
    type: null,
  },
  {
    label: "Keris",
    key: "keris",
    path: "/keris",
    icon: <FileOutlined />,
    children: null,
    type: null,
  },
];

export default sidebarMenu;
