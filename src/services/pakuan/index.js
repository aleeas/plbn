import axios from 'axios'
import { fetcher, API } from '../index'

export const uploadFiles = async (token, payload)=> {
  try {
    const { data: response } = await fetcher.post(API.UPLOAD.POST.PAKUAN(token), payload);
    return response;
  } catch (error) {
    if (axios.isAxiosError(error)) {
      throw error.response?.data;
    }
    throw error;
  }
};

