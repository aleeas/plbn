import CustomLayout from "@/components/layouts";
import "@/styles/globals.css";

export default function App({ Component, pageProps }) {
  return (
    <CustomLayout>
      <Component {...pageProps} />
    </CustomLayout>
  );
}
