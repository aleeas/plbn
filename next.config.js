/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: false,
  env: {
    BASE_URL: process.env.NEXT_PUBLIC_BASE_URL,
  },
  images: {
    domains: ["https://api.siliwangi.cloud", "**"],
  },
  async rewrites() {
    return [
      {
        source: '/:path*',
        destination: 'https://api.siliwangi.cloud/:path*',
      },
    ]
  },
};

module.exports = nextConfig;
