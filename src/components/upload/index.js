import convertBase64 from "@/utils/base64Formatter";
import checkImageFileSize from "@/utils/checkImageSize";
import { PlusOutlined } from "@ant-design/icons";
import { Form, Modal, Upload } from "antd";
import { useState } from "react";

const getBase64 = (file) =>
  new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = (error) => reject(error);
  });

export default function UploadFiles({ setBase64Image, setFileList, fileList }) {
  const [previewOpen, setPreviewOpen] = useState(false);
  const [previewImage, setPreviewImage] = useState("");
  const [previewTitle, setPreviewTitle] = useState("");
  const handleCancelPreview = () => setPreviewOpen(false);

  const allowedType = ["image/png", "image/jpeg", "image/jpg"];

  const props = {
    onChange: async (target) => {
      const isLt5M = target.file.size / 1024 / 1024 <= 5;
      if (!isLt5M) {
        setFileList([]);
      } else {
        const base64 = await convertBase64(target.file.originFileObj);
        setBase64Image(base64);
        setFileList(target.fileList);
      }
    },
  };
  const handlePreview = async (file) => {
    if (!file.url && !file.preview) {
      file.preview = await getBase64(file.originFileObj);
    }
    setPreviewImage(file.url || file.preview);
    setPreviewOpen(true);
    setPreviewTitle(
      file.name || file.url.substring(file.url.lastIndexOf("/") + 1)
    );
  };

  
  const normFile = (e) => {
    if (Array.isArray(e)) {
      return e;
    }
    return e && e.fileList;
  };

  const uploadButton = (
    <div>
      <PlusOutlined />
      <div
        style={{
          marginTop: 8,
        }}
      >
        Upload
      </div>
    </div>
  );
  return (
    <>
      <Form.Item
        label="Upload Picture"
        name="face_bytes"
        getValueFromEvent={normFile}
        rules={[
          {
            required: true,
            message: "Upload Picture is required!",
          },
        ]}
      >
        <Upload
          accept={allowedType}
          beforeUpload={checkImageFileSize}
          fileList={fileList}
          listType="picture-card"
          onPreview={handlePreview}
          maxCount={1}
          {...props}
        >
          {fileList.length >= 1 ? null : uploadButton}
        </Upload>
      </Form.Item>

      <Modal
        open={previewOpen}
        title={previewTitle}
        footer={null}
        onCancel={handleCancelPreview}
      >
        <img
          alt="previewImage"
          style={{
            width: "100%",
          }}
          src={previewImage}
        />
      </Modal>
    </>
  );
}
