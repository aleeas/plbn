import React, { useEffect, useState } from "react";
import { Breadcrumb, Layout, Menu } from "antd";
import { useRouter } from "next/router";
import sidebarMenu from "@/constant/sidebar";

const { Header, Content, Footer, Sider } = Layout;

export default function CustomLayout({ children }) {
  const router = useRouter();
  const [collapsed, setCollapsed] = useState(false);
  const [breadcrumbValue, setBreadcrumbValue] = useState();
  const [selectedKey, setSelectedKey] = useState();

  const onClickMenu = (e) => {
    const path = e.key;
    const clicked = sidebarMenu.find((_item) => _item.key === e.key);
    setBreadcrumbValue(path === "/" ? "Home" : path);
    setSelectedKey(e.key);
    router.push(clicked.path);
  };

  useEffect(() => {
    if (router.pathname !== "/") {
      setSelectedKey(
        sidebarMenu.find((_item) => router.pathname.startsWith(_item.path)).key
      );
    }
  }, [router]);

  return (
    <Layout
      style={{
        minHeight: "100vh",
      }}
    >
      <Sider
        collapsible
        collapsed={collapsed}
        onCollapse={(value) => setCollapsed(value)}
      >
        <div className="demo-logo-vertical">LBN</div>
        <Menu
          onClick={onClickMenu}
          selectedKeys={[selectedKey]}
          theme="dark"
          mode="inline"
          items={sidebarMenu}
        />
      </Sider>
      <Layout>
        <Header style={{ backgroundColor: "#fff" }} />
        <Content
          style={{
            margin: "0 16px",
          }}
        >
          <Breadcrumb
            items={[
              {
                title: (
                  <>
                    Pages / <span>{breadcrumbValue}</span>
                  </>
                ),
              },
            ]}
            style={{ margin: "16px 0", textTransform: "capitalize" }}
          />

          <div
            style={{
              padding: 24,
              minHeight: "100%",
              background: "#fff",
            }}
          >
            {children}
          </div>
        </Content>
        <Footer
          style={{
            textAlign: "center",
          }}
        >
          LBN copyright@2023
        </Footer>
      </Layout>
    </Layout>
  );
}
