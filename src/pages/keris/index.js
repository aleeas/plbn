import UploadFiles from "@/components/upload";
import { matchData } from "@/services/keris";
import convertBase64String from "@/utils/base64Encode";
import {
  Button,
  Modal,
  Form,
  message,
  Checkbox,
  Table,
  Input,
} from "antd";
import React, { Fragment, useState } from "react";

export default function Pakuan() {
  const [form] = Form.useForm();
  const [messageApi, contextHolder] = message.useMessage();
  const [fileList, setFileList] = useState([]);
  const [dataUsers, setDataUsers] = useState([]);
  const [base64Image, setBase64Image] = useState();
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [isModalScoreOpen, setIsModalScoreOpen] = useState(false);
  const [isChecked, setIsChecked] = useState(false);

  const transformData = dataUsers?.biometrik
    ? dataUsers?.biometrik[0].grid_details
        .flatMap(Object.entries)
        .map(([bagian, value]) => ({ ...value, bagian }))
    : null;

  const columns = [
    {
      title: "Bagian",
      dataIndex: "bagian",
      key: "bagian",
    },
    {
      title: "Jumlah Grid Kandidat",
      dataIndex: "jml_grid_kandidat",
      key: "jml_grid_kandidat",
    },
    {
      title: "Jumlah Grid Enrollment",
      dataIndex: "jml_grid_enrollment",
      key: "jml_grid_enrollment",
    },
    {
      title: "Presentase Kemiripan",
      dataIndex: "presentase_kemiripan",
      key: "presentase_kemiripan",
    },
  ];

  const showModal = () => {
    setIsModalOpen(true);
  };
  const showModalScore = () => {
    setIsModalScoreOpen(true);
  };
  const handleCancelModalScore = () => {
    setIsModalScoreOpen(false);
  };
  const handleCancel = () => {
    setIsModalOpen(false);
  };
  const handleSubmit = async (values) => {
    let raray = { biometrik: isChecked, pic: base64Image };
    let token = convertBase64String({
      _token: "MGY5YTJkZGMtN2ViNy00ZTA0LWExNDctOGU0ZWVjZmM2NWI4",
      ask_for: values.ask_for,
    });
    try {
      const response = await matchData(token, raray);
      if (response.status === "diterima") {
        form.resetFields();
        setFileList([]);
        setIsModalOpen(false);
        setDataUsers(response.fr_data[0].users[0]);
        messageApi.open({
          type: "success",
          content: "Data diterima",
        });
      } else if (response.status === "requested_user_not_found") {
        messageApi.open({
          type: "warning",
          content: "User not found",
        });
      }
    } catch (error) {
      console.log(error);
      messageApi.open({
        type: "error",
        content: error.message,
      });
    }
  };


  const tableScore = () => {
    const columns = [
      {
        title: "Min Skor",
        dataIndex: "min_skor",
        key: "min_skor",
      },
      {
        title: "Maks Skor",
        dataIndex: "maks_skor",
        key: "maks_skor",
      },
      {
        title: "Anomaly",
        dataIndex: "anomaly",
        key: "anomaly",
      },
      {
        title: "Source Pic",
        dataIndex: "source_pic",
        key: "source_pic",
      },
      {
        title: "Pic Type",
        dataIndex: "pic_type",
        key: "pic_type",
      },
    ];
    return (
      <Table
        columns={columns}
        dataSource={dataUsers.biometrik}
        pagination={false}
      />
    );
  };

  return (
    <Fragment>
      {contextHolder}
      <div style={{ textAlign: "center", marginBottom: 20 }}>
        <Button type="primary" onClick={showModal}>
          Match Picture
        </Button>
      </div>

      {dataUsers?.biometrik && (
        <>
          <Table columns={columns} dataSource={transformData} pagination={false}/>
          <div style={{ textAlign: "center", margin: "20px 0" }}>
            <Button type="primary" onClick={showModalScore}>
              Show Score
            </Button>
          </div>
        </>
      )}

      <Modal
        title="Show Score"
        open={isModalScoreOpen}
        footer={null}
        onCancel={handleCancelModalScore}
      >
        {tableScore()}
      </Modal>

      <Modal
        title="Match Picture"
        open={isModalOpen}
        footer={null}
        onCancel={handleCancel}
      >
        <Form
          layout={"vertical"}
          form={form}
          onFinish={handleSubmit}
          initialValues={{
            layout: "vertical",
          }}
        >
          <Form.Item name="biometrik" valuePropName="checked">
            <Checkbox onChange={() => setIsChecked(!isChecked)}>
              Biometrik
            </Checkbox>
          </Form.Item>
          <Form.Item label="Ask For" name="ask_for">
            <Input placeholder="ask_for user" />
          </Form.Item>
          <UploadFiles
            setBase64Image={setBase64Image}
            setFileList={setFileList}
            fileList={fileList}
          />
          <Form.Item>
            <Button type="primary" htmlType="submit">
              Submit
            </Button>
          </Form.Item>
        </Form>
      </Modal>
    </Fragment>
  );
}
