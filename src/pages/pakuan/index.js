import UploadFiles from "@/components/upload";
import { uploadFiles } from "@/services/pakuan";
import convertBase64String from "@/utils/base64Encode";
import { Button, Modal, Form, Input, message } from "antd";
import React, { Fragment, useState } from "react";

export default function Pakuan() {
  const [form] = Form.useForm();
  const [messageApi, contextHolder] = message.useMessage();
  const [fileList, setFileList] = useState([]);
  const [base64Image, setBase64Image] = useState();
  const [isModalOpen, setIsModalOpen] = useState(false);

  const token = convertBase64String({
    _token: "MGY5YTJkZGMtN2ViNy00ZTA0LWExNDctOGU0ZWVjZmM2NWI4",
  });

  const showModal = () => {
    setIsModalOpen(true);
  };
  const handleCancel = () => {
    setIsModalOpen(false);
  };
  const handleSubmit = async (values) => {
    let data = { face_bytes: base64Image };
    if (values.user) {
      data = { ...data, user: values.user };
    }
    if (values.source_pic) {
      data = { ...data, source_pic: values.source_pic };
    }
    if (values.pic_type) {
      data = { ...data, pic_type: values.pic_type };
    }
    try {
      const response = await uploadFiles(token, { raray: data });
      form.resetFields();
      setFileList([]);
      setIsModalOpen(false);
      if (response.status === 200) {
        messageApi.open({
          type: "success",
          content: response.message,
        });
      } else if (response.status === 400) {
        messageApi.open({
          type: "warning",
          content: response.message,
        });
      }
    } catch (error) {
      console.log(error);
      messageApi.open({
        type: "error",
        content: error.message,
      });
    }
  };


  return (
    <Fragment>
      {contextHolder}
      <div style={{ textAlign: "center" }}>
        <Button type="primary" onClick={showModal}>
          Add Picture
        </Button>
      </div>

      <Modal
        title="Add Picture"
        open={isModalOpen}
        footer={null}
        onCancel={handleCancel}
      >
        <Form
          layout={"vertical"}
          form={form}
          onFinish={handleSubmit}
          initialValues={{
            layout: "vertical",
          }}
        >
          <Form.Item label="User" name="user">
            <Input
              placeholder="identity user like nik or no hp"
              maxLength={32}
            />
          </Form.Item>
          <Form.Item label="Source Picture" name="source_pic">
            <Input placeholder="wikipedia, google etc" />
          </Form.Item>
          <Form.Item label="Picture Type" name="pic_type">
            <Input placeholder="id_card or etc" />
          </Form.Item>

          <UploadFiles
            setBase64Image={setBase64Image}
            setFileList={setFileList}
            fileList={fileList}
          />

          <Form.Item>
            <Button type="primary" htmlType="submit">
              Submit
            </Button>
          </Form.Item>
        </Form>
      </Modal>
    </Fragment>
  );
}
